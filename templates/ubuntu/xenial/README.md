---
name: Universal - Ubuntu Xenial
description: Develop in your browser with VS Code and Ubuntu 16.04
tags: [local, docker]
---

# Universal - Ubuntu Xenial

**Ubuntu 16.04 is outdated and has gone out of standard support!**
**Software included may be too old for some projects.**

Develop in your browser with VS Code. Bring your dotfiles with you.

## How to use:

After setting up a workspace, click "VSCode" to open your IDE. You are ready to go!

## "VSCode is offline" or "502 Bad Gateway" error:

It may take a moment for the IDE to go online. If it doesn't, try opening an issue [on the project repository](https://gitlab.com/8Bitz0/coder-universal-template).
