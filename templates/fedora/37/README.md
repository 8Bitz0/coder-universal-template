---
name: Universal - Fedora 37
description: Develop in your browser with VS Code and Fedora
tags: [local, docker]
---

# Universal - Fedora 37 

Develop in your browser with VS Code. Bring your dotfiles with you.

## How to use:

After setting up a workspace, click "VSCode" to open your IDE. You are ready to go!

## "VSCode is offline" or "502 Bad Gateway" error:

It may take a moment for the IDE to go online. If it doesn't, try opening an issue [on the project repository](https://gitlab.com/8Bitz0/coder-universal-template).
