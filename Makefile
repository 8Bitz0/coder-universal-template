build:
	# Fedora 37 (Stable)
	docker build -t 8bitz0/universal:fedora-37 images/fedora/37/
	# Fedora 36 (Stable)
	docker build -t 8bitz0/universal:fedora-36 images/fedora/36/
	# Rocky Linux 9 (Blue Onyx)
	docker build -t 8bitz0/universal:rocky-9 images/rocky/9/
	# Rocky Linux 8 (Green Obsidian)
	docker build -t 8bitz0/universal:rocky-8 images/rocky/8/
	# Ubuntu Jammy (22.04 LTS)
	docker build -t 8bitz0/universal:jammy images/ubuntu/jammy
	# Ubuntu Focal (20.04 LTS)
	docker build -t 8bitz0/universal:focal images/ubuntu/focal
	# Ubuntu Bionic (18.04 LTS)
	docker build -t 8bitz0/universal:bionic images/ubuntu/bionic
	# Ubuntu Xenial (16.04 LTS) (EOL)
	#docker build -t 8bitz0/universal:xenial images/ubuntu/xenial

push: build
	# Fedora 37 (Stable)
	docker push 8bitz0/universal:fedora-37
	# Fedora 36 (Stable)
	docker push 8bitz0/universal:fedora-36
	# Rocky Linux (Blue Onyx)
	docker push 8bitz0/universal:rocky-9
	# Rocky Linux (Green Obsidian)
	docker push 8bitz0/universal:rocky-8
	# Ubuntu Jammy (22.04 LTS)
	docker push 8bitz0/universal:jammy
	# Ubuntu Focal (20.04 LTS)
	docker push 8bitz0/universal:focal
	# Ubuntu Bionic (18.04 LTS)
	docker push 8bitz0/universal:bionic
	# Ubuntu Xenial (16.04 LTS) (EOL)
	#docker push 8bitz0/universal:xenial
